package com.example.eventtracker.view.event_type

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.room.Room
import com.example.eventtracker.R
import com.example.eventtracker.database.EventDatabase
import com.example.eventtracker.database.entity.EventType
import com.example.eventtracker.databinding.FragmentEditEventTypeBinding
import com.example.eventtracker.databinding.FragmentFirstBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_NAME = "param1"
private const val ARG_DESCRIPTION = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [EditEventTypeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EditEventTypeFragment : Fragment() {
    private var _binding: FragmentEditEventTypeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    // TODO: Rename and change types of parameters
    private var name: String? = null
    private var description: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            name = it.getString(ARG_NAME)
            description = it.getString(ARG_DESCRIPTION)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEditEventTypeBinding.inflate(inflater, container, false)

        binding.confirm.setOnClickListener { view ->
            val name: String = binding.editTextEventTypeName.text.toString()
            val description: String = binding.editTextTextEventTypeDescription.text.toString()
            val eventType: EventType = EventType(0, name, description)

            val insertIntoDb = GlobalScope.launch {
                val db = Room.databaseBuilder(
                    binding.root.context,
                    EventDatabase::class.java,
                    "event_database"
                ).build()

                val eventTypeDao = db.eventTypeDao()
                eventTypeDao.insertAll(eventType)
            }

            runBlocking {
                insertIntoDb.join()
            }

            val action = R.id.action_editEventTypeFragment_to_FirstFragment
            findNavController().navigate(action)
        }

        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment EditEventTypeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            EditEventTypeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_NAME, param1)
                    putString(ARG_DESCRIPTION, param2)
                }
            }
    }
}