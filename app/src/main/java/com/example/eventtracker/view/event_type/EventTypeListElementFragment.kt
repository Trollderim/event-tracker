package com.example.eventtracker.view.event_type

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.eventtracker.R

class EventTypeListElementFragment() : Fragment() {
    companion object {
        fun newInstance() = EventTypeListElementFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_event_type_list_element, container, false)

        return view
    }
}