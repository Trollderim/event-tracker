package com.example.eventtracker.view.event_type

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.eventtracker.database.entity.Event
import com.example.eventtracker.databinding.FragmentEventTypeDetailBinding
import com.example.eventtracker.databinding.FragmentEventTypeDetailListItemBinding
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

/**
 * [RecyclerView.Adapter] that can display a [Event].
 */
class EventListRecyclerAdapter(
    private val values: List<Event>
) : RecyclerView.Adapter<EventListRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            FragmentEventTypeDetailListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.timeView.text = Instant.ofEpochSecond(item.time).atZone(ZoneId.systemDefault()).toLocalDateTime().format(
            DateTimeFormatter.ISO_LOCAL_DATE)
        holder.notesView.text = item.notes
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentEventTypeDetailListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val timeView: TextView = binding.eventTime
        val notesView: TextView = binding.eventNotes

        override fun toString(): String {
            return super.toString() + " '" + timeView.text + " " + notesView.text + "'"
        }
    }

}