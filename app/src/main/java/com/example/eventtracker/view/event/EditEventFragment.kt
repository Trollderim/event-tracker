package com.example.eventtracker.view.event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.room.Room
import com.example.eventtracker.R
import com.example.eventtracker.database.EventDatabase
import com.example.eventtracker.database.entity.Event
import com.example.eventtracker.database.entity.EventType
import com.example.eventtracker.databinding.FragmentEditEventBinding
import kotlinx.coroutines.*
import java.time.Instant
import java.time.LocalDateTime
import java.time.LocalDateTime.parse
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

private const val ARG_TIME = "time"
private const val ARG_NOTES = "notes"
private const val ARG_EVENT_TYPE_ID = "eventTypeId"

/**
 * A simple [Fragment] subclass.
 * Use the [EditEventFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EditEventFragment : Fragment() {
    private var _binding: FragmentEditEventBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var time: LocalDateTime? = null
    private var notes: String? = null
    private var eventTypeId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { it ->
            time = parse(it.getString(ARG_TIME))
            notes = it.getString(ARG_NOTES)
            eventTypeId = it.getLong(ARG_EVENT_TYPE_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentEditEventBinding.inflate(inflater, container, false)

        binding.buttonConfirm.setOnClickListener {
            val time: LocalDateTime = Instant.ofEpochMilli(binding.eventDatePicker.date).atZone(
                ZoneId.systemDefault()).toLocalDateTime()
            val notes: String = binding.editTextNotes.text.toString()
            val event = Event(0, this.eventTypeId, time.toEpochSecond(ZoneOffset.UTC), notes)

            CoroutineScope(Dispatchers.IO).launch {
                val db = Room.databaseBuilder(
                    binding.root.context,
                    EventDatabase::class.java,
                    "event_database"
                ).build()

                val eventDao = db.eventDao()
                eventDao.insertAll(event)
            }

            val action = R.id.action_editEventFragment_to_FirstFragment
            findNavController().navigate(action)
        }

        binding.buttonCancel.setOnClickListener {
            val action = R.id.action_editEventFragment_to_FirstFragment
            findNavController().navigate(action)
        }

        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance(time: LocalDateTime, notes: String, eventType: EventType) =
            EditEventFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_TIME, time.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                    putString(ARG_NOTES, notes)
                    putLong(ARG_EVENT_TYPE_ID, eventType.eventTypeId)
                }
            }
    }
}