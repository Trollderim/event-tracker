package com.example.eventtracker.view.event_type

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.eventtracker.R
import com.example.eventtracker.database.EventDatabase
import com.example.eventtracker.database.entity.Event
import com.example.eventtracker.database.entity.EventType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private val ARG_EVENT_TYPE_ID = "eventTypeId"

/**
 * A simple [Fragment] subclass.
 * Use the [EventTypeDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EventTypeDetailFragment : Fragment() {
    private var eventTypeId: Long = 0
    private var eventType: EventType? = null
    private var events: MutableList<Event> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            eventTypeId = it.getLong(ARG_EVENT_TYPE_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_event_type_detail, container, false)

        CoroutineScope(Dispatchers.IO).launch {
            fetchEventType(eventTypeId)
        }

        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerViewEvent)
        if (recyclerView is RecyclerView) {
            with(recyclerView) {
                layoutManager = LinearLayoutManager(context)
                adapter = EventListRecyclerAdapter(events)
            }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        CoroutineScope(Dispatchers.IO).launch {
            fetchEvents(eventTypeId)
        }
    }

    private fun fetchEventType(eventTypeId: Long) {
        val db = Room.databaseBuilder(
            requireContext(),
            EventDatabase::class.java,
            "event_database"
        ).build()

        val eventTypeDao = db.eventTypeDao()
        this.eventType = eventTypeDao.getSingleEventType(eventTypeId)

        requireView().findViewById<TextView>(R.id.textViewNameEventTypeDetail).text = eventType?.name
        requireView().findViewById<TextView>(R.id.textViewDescriptionEventTypeDetail).text = eventType?.description
    }

    private fun fetchEvents(eventTypeId: Long) {
        val db = Room.databaseBuilder(
            requireContext(),
            EventDatabase::class.java,
            "event_database"
        ).build()

        val eventDao = db.eventDao()
        this.events.clear()
        this.events.addAll(eventDao.getEventsForEventType(eventTypeId).reversed())
        if (this.view is RecyclerView) {
            (this.view as RecyclerView).adapter?.notifyDataSetChanged()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(eventTypeId: Long) =
            EventTypeDetailFragment().apply {
                arguments = Bundle().apply {
                    putLong(ARG_EVENT_TYPE_ID, eventTypeId)
                }
            }
    }
}