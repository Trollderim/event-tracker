package com.example.eventtracker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.eventtracker.database.EventDatabase
import com.example.eventtracker.database.entity.EventType
import com.example.eventtracker.databinding.FragmentFirstBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var eventTypes: MutableList<EventType> = ArrayList()
    private lateinit var adapter: EventTypeListAdapter

    class EventTypeListAdapter(private val dataSet: List<EventType>) :
        RecyclerView.Adapter<EventTypeListAdapter.ViewHolder>() {
        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val textViewName: TextView
            val textViewDescription: TextView

            val trackInstance: Button

            init {
                textViewName = view.findViewById(R.id.textViewName)
                textViewDescription = view.findViewById(R.id.textViewDescription)

                trackInstance = view.findViewById(R.id.trackInstance)
            }
        }

        // Create new views (invoked by the layout manager)
        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
            // Create a new view, which defines the UI of the list item
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.fragment_event_type_list_element, viewGroup, false)

            return ViewHolder(view)
        }

        // Replace the contents of a view (invoked by the layout manager)
        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

            // Get element from your dataset at this position and replace the
            // contents of the view with that element
            viewHolder.textViewName.text = dataSet[position].name
            viewHolder.textViewDescription.text = dataSet[position].description

            viewHolder.trackInstance.setOnClickListener { view ->
                val bundle = Bundle()
                bundle.putString("time", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                bundle.putString("notes", "")
                bundle.putLong("eventTypeId", dataSet[position].eventTypeId)

                val action = R.id.action_FirstFragment_to_editEventFragment
                view.findNavController().navigate(action, bundle)
            }

            viewHolder.itemView.setOnClickListener { view ->
                val bundle = Bundle()
                bundle.putLong("eventTypeId", dataSet[position].eventTypeId)

                val action = R.id.action_FirstFragment_to_eventTypeDetailFragment
                view.findNavController().navigate(action, bundle)
            }
        }

        // Return the size of your dataset (invoked by the layout manager)
        override fun getItemCount() = dataSet.size
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)

        binding.createEventTypeButton.setOnClickListener {
            val action = R.id.action_FirstFragment_to_editEventTypeFragment
            findNavController().navigate(action)
        }

        adapter = EventTypeListAdapter(this.eventTypes)
        val recyclerView = binding.root.findViewById<RecyclerView>(R.id.recyclerViewEventType)
        recyclerView.adapter = adapter

        val layoutManager = LinearLayoutManager(binding.root.context)
        recyclerView.layoutManager = layoutManager

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        CoroutineScope(Dispatchers.IO).launch {
            fetchEventTypes()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun fetchEventTypes() {
        val db = Room.databaseBuilder(
            binding.root.context,
            EventDatabase::class.java,
            "event_database"
        ).build()

        val eventTypeDao = db.eventTypeDao()
        this.eventTypes.clear()
        val previousSize = this.eventTypes.size
        this.eventTypes.addAll(eventTypeDao.allEventTypes)

        for (i in previousSize..this.eventTypes.size) {
            adapter.notifyItemInserted(i)
        }

        val eventDao = db.eventDao()

        this.eventTypes.forEach{ eventType ->
            System.out.println(eventDao.getEventsForEventType(eventType.eventTypeId))
        }
    }
}