package com.example.eventtracker.database.dao;

import com.example.eventtracker.database.entity.EventType;
import com.example.eventtracker.database.relations.EventTypeWithEvents;
import com.example.eventtracker.database.relations.EventTypesWithTraits;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

@Dao
public interface EventTypeDao {
    @Query("SELECT * from EventType")
    List<EventType> getAllEventTypes();

    @Query("SELECT * from EventType where eventTypeId = :id")
    EventType getSingleEventType(Long id);

    @Insert
    void insertAll(EventType... eventTypes);
    @Delete
    void delete(EventType eventType);
    @Update
    void update(EventType eventType);

    @Transaction
    @Query("SELECT * FROM EventType")
    List<EventTypeWithEvents> getEventTypesWithEvents();

    @Transaction
    @Query("SELECT * FROM EventType")
    List<EventTypesWithTraits> getEventTypesWithTraits();
}
