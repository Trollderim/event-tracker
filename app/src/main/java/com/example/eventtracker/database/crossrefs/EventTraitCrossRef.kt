package com.example.eventtracker.database.crossrefs

import androidx.room.Entity

@Entity(primaryKeys = ["eventId", "traitId"])
data class EventTraitCrossRef(
    val eventId: Long,
    val traitId: Long
)