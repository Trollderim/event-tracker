package com.example.eventtracker.database.dao

import androidx.room.*
import com.example.eventtracker.database.entity.Event
import com.example.eventtracker.database.entity.EventType
import com.example.eventtracker.database.relations.EventsWithTraits

@Dao
interface EventDao {
    @Transaction
    @Query("SELECT * from Event where eventTypeId=:eventTypeId")
    fun getEventsForEventType(eventTypeId: Long): List<Event>

    @Transaction
    @Query("SELECT * from Event")
    fun getEventsWithTraits(): List<EventsWithTraits>

    @Insert
    fun insertAll(vararg events: Event?)

    @Delete
    fun delete(event: Event?)

    @Update
    fun update(event: Event?)
}