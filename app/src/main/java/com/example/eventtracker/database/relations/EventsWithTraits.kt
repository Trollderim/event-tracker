package com.example.eventtracker.database.relations

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.eventtracker.database.crossrefs.EventTraitCrossRef
import com.example.eventtracker.database.entity.Event
import com.example.eventtracker.database.entity.Trait

data class EventsWithTraits (
    @Embedded val event: Event,
    @Relation(
        parentColumn = "eventId",
        entityColumn = "traitId",
        associateBy = Junction(EventTraitCrossRef::class)
    )
    val traits: List<Trait>
    )