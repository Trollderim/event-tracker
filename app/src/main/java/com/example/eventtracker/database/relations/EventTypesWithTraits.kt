package com.example.eventtracker.database.relations

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.eventtracker.database.crossrefs.EventTraitCrossRef
import com.example.eventtracker.database.crossrefs.TraitEventTypeCrossRef
import com.example.eventtracker.database.entity.EventType
import com.example.eventtracker.database.entity.Trait

data class EventTypesWithTraits (
    @Embedded val eventType: EventType,
    @Relation(
        parentColumn = "eventTypeId",
        entityColumn = "traitId",
        associateBy = Junction(TraitEventTypeCrossRef::class)
    )
    val traits: List<Trait>
    )