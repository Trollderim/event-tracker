package com.example.eventtracker.database.crossrefs

import androidx.room.Entity

@Entity(primaryKeys = ["traitId", "eventTypeId"])
data class TraitEventTypeCrossRef(
    val traitId: Long,
    val eventTypeId: Long
)