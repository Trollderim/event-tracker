package com.example.eventtracker.database

import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.eventtracker.database.crossrefs.EventTraitCrossRef
import com.example.eventtracker.database.crossrefs.TraitEventTypeCrossRef
import com.example.eventtracker.database.dao.EventDao
import com.example.eventtracker.database.dao.EventTypeDao
import com.example.eventtracker.database.entity.Event
import com.example.eventtracker.database.entity.EventType
import com.example.eventtracker.database.entity.Trait

@Database(entities = [Event::class, EventType::class, Trait::class, EventTraitCrossRef::class, TraitEventTypeCrossRef::class],
    version = 1)
abstract class EventDatabase : RoomDatabase() {
    abstract fun eventTypeDao(): EventTypeDao

    abstract fun eventDao(): EventDao
}