package com.example.eventtracker.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class EventType(
    @PrimaryKey(autoGenerate = true)
    val eventTypeId: Long,
    val name: String,
    val description: String
) {

}