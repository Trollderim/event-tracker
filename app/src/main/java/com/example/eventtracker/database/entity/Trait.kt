package com.example.eventtracker.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Trait(
    @PrimaryKey(autoGenerate = true)
    val traitId: Long,
    val name: String,
    val description: String) {

}