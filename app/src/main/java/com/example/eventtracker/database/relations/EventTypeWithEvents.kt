package com.example.eventtracker.database.relations

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation
import com.example.eventtracker.database.entity.Event
import com.example.eventtracker.database.entity.EventType

@Entity
class EventTypeWithEvents (
    @Embedded
    val eventType: EventType,
    @Relation(
        parentColumn = "eventTypeId",
        entityColumn = "eventId"
    )
    val events: List<Event>
    )