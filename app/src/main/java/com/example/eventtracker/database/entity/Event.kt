package com.example.eventtracker.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Timestamp

@Entity
data class Event(
    @PrimaryKey(autoGenerate = true)
    val eventId: Long,

    val eventTypeId: Long,
    val time: Long,
    val notes: String,
) {

}